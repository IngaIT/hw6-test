import products, {
  initialState,
  setFavorites,
  setProductsInCart,
  handleAddCartClick,
  handleDeleteCartClick,
  toggleFavorites,
} from "../redux/reducers/productsThunk";

describe("productsThunk", () => {
  it("should return default state when passed an empty action", () => {
    const result = products(undefined, { type: "" });
    expect(result).toEqual(initialState);
  });

  it('should set favorite product item with "setFavorites" action', () => {
    const action = { type: setFavorites.type, payload: [1, 2, 3] };
    const result = products(initialState, action);
    expect(result.favorites).toEqual([1, 2, 3]);
  });

  it('should set cart product item with "setProductsInCart" action', () => {
    const action = {
      type: setProductsInCart.type,
      payload: [{ id: 1, count: 2 }],
    };
    const result = products(initialState, action);
    expect(result.productsInCart).toEqual([{ id: 1, count: 2 }]);
  });

  it('should add to cart the product item with "handleAddCartClick" action', () => {
    const action = {
      type: handleAddCartClick.type,
      payload: 1,
    };
    const result = products(initialState, action);
    expect(result.productsInCart).toEqual([{ id: 1, count: 1 }]);
  });

  it('should add to cart addition product item with "handleAddCartClick" action', () => {
    const action = {
      type: handleAddCartClick.type,
      payload: 1,
    };
    const result = products(
      { ...initialState, productsInCart: [{ id: 1, count: 1 }] },
      action
    );
    expect(result.productsInCart).toEqual([{ id: 1, count: 2 }]);
  });

  it('should delete from cart the product item with "handleDeleteCartClick" action', () => {
    const action = {
      type: handleDeleteCartClick.type,
      payload: 1,
    };
    const result = products(
      { ...initialState, productsInCart: [{ id: 1, count: 1 }] },
      action
    );
    expect(result.productsInCart).toEqual([]);
  });

  it('should delete from cart one product item with "handleAddCartClick" action', () => {
    const action = {
      type: handleDeleteCartClick.type,
      payload: 1,
    };
    const result = products(
      { ...initialState, productsInCart: [{ id: 1, count: 2 }] },
      action
    );
    expect(result.productsInCart).toEqual([{ id: 1, count: 1 }]);
  });
});
