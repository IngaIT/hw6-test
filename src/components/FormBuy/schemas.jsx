import * as yup from "yup";

export const validationSchema = yup.object().shape({
  name: yup
    .string()
    .matches(/^[a-zA-Za-яА-Я]+$/, "Введіть лише кириличні або латинські літери")
    .min(3, "Ім'я повинно бути більше ніж 3 символи")
    .required("Це обов'язкове поле"),
  lastName: yup
    .string()
    .matches(
      /^[a-zA-Zа-яА-Я ]+$/,
      "Введіть лише кириличні або латинські літери"
    )
    .min(3, "Прізвище повинно бути більше ніж 3 символи")
    .required("Це обов'язкове поле"),
  age: yup
    .number()
    .typeError("Поле повинно бути числом")
    .min(13, "Вік повинен бути більше 12")
    .required("Це обов'язкове поле"),
  address: yup
    .string()
    .min(3, "Адреса повинна бути більше ніж 3 символи")
    .required("Це обов'язкове поле"),
  phone: yup
    .string()
    .matches(
      /^\(\d{3}\)\d{3}-\d{2}-\d{2}$/,
      "Номер телефону повинен бути у форматі (###)###-##-##"
    )
    .required("Це обов'язкове поле"),
});
